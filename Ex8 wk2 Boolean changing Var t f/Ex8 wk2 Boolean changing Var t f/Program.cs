﻿using System;

namespace Ex8_wk2_Boolean_changing_Var_t_f
{
    class MainClass
    {
       public static void Main(string[] args)
        {
            Console.WriteLine("Tomatoes are a fruit!");
            Console.WriteLine("Do you agree? Type in the True or False");
            var answer = bool.Parse(Console.ReadLine());
            var result = "";

            if (answer)
            {
                result = "You are correct, but many people will probably argue about it with you.";
            }
            else
            {
                result = "Sadly you belong to the group of disblievers";
            }

            Console.WriteLine($"{result}");
            Console.WriteLine($"Knowledge is Tomatoes are a fruit, Wisdom is not to put it into a fruitsalad!");

            }
        }
    }
