﻿using System;

namespace Ex5_wk2_if_statement_boolean_option_score
{
    class MainClass
    {
       public static void Main(string[] args)
        {
            var intro = "Hi, see how many of these you can get correct by typing in true or false";
            var question1 = $"1 + 1 = 5";
            var question2 = $"2 + 2 = 4";
            var question3 = $"3 x 3 = 8";
            var question4 = $"3 x 3 = 9";
            var question5 = $"5 - 5 = 0";
            var score = 0;
            var correct = true;

            Console.WriteLine($"{intro}");

            if (correct)
            {
                Console.WriteLine($"{question1}");
                if (Console.ReadLine() == "false")
                        {
                    score += 1;
                    correct = true;
                }
                else {
                    Console.WriteLine($"Hope you enjoyed your game.  Your score was {score}");
                    correct = false;
                }
            }
        }
    }
}
