﻿using System;

namespace Ex9_wk2_The_Mashup
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            var again = true;

            do
            {
                Console.WriteLine("Choose number...");

                var counter = int.Parse(Console.ReadLine());
                var i = 0;

                if (again)
                {
                    for (i = 0; i < counter; i++)
                    {
                        var j = i + 1;
                        Console.WriteLine($"This is line {j} ");
                    }
                }

                if (i == counter)
                { 
                    Console.WriteLine($"Do you want to choose another number? - Type Y or N");
                var answer = Console.ReadLine();

                if ((answer == "y") || (answer == "y"))
                {
                    again = true;
                }
                else
                {
                    again = false;
                    Console.WriteLine($"Thank you using this app");
                }
            }
            } while (again == true);
        }
}
}


