﻿using System;

namespace Ex4_wk2
{
    class MainClass
    {
        static void Main(string[] args)
        {
            var counter = 5;
            var i = 0;

            for (i = 0; i < counter; i++)
            {
                var J = i + 1;
                Console.WriteLine($"This is the line {J}");
            }
        }
    }
}
