﻿using System;

namespace Ex6__wk2_while_loop
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            var counter = 5;
            var index = 0;

            while (index < counter)
            {
                var a = index + 1;
                Console.WriteLine($"This is line {a}");

                index++;

            
            }
        }
    }
}
