﻿using System;


namespace Ex7_wk2_DoWhile
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Please enter in a whole number...");

            var index = int.Parse(Console.ReadLine());
            var counter = 20;

            do
            {
                var j = index + 1;

                Console.WriteLine($"This is Line {j}");

                index++;
            } while (index < counter);


        }
    }
}
